FROM node:12
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
RUN npm audit fix
COPY . .
EXPOSE 3000
CMD [ "node","./bin/www" ]
